import os
import shutil
from amarium.utils import copy_dir
from amarium.checks import check_delete_dir
import subprocess

from amarium.utils import search_subdirs
from amarium.checks import check_delete_dir


def test_file_and_dir_func():

    test_run_dir = "tests/cli_files"
    if os.path.isdir(test_run_dir):
        shutil.rmtree(test_run_dir)
    os.mkdir(test_run_dir)
    copy_dir("tests/check_files", test_run_dir)
    subprocess.run(["bash", "install.sh"], check=True)

    file_name = "tests/cli_files/sample.py"
    num = 100

    out = subprocess.run(
        [f"prettify-py", "format-file", f"{file_name}", "-n", f"{num}"],
        capture_output=True,
        check=True,
    )
    assert "ModuleNotFoundError: No module named 'lia'" not in str(out)
    assert "No such option:" not in str(out)

    out = subprocess.run(
        ["prettify-py", "format-dir", f"{test_run_dir}", "-n", f"{num}"],
        capture_output=True,
        check=True,
    )

    assert "ModuleNotFoundError: No module named 'lia'" not in str(out)
    assert "No such option:" not in str(out)

    check_delete_dir("tests/cli_files")


def test_src_structure():

    test_run_dir = "tests/cli_files"
    if os.path.isdir(test_run_dir):
        shutil.rmtree(test_run_dir)
    os.mkdir(test_run_dir)
    copy_dir("tests/src", test_run_dir)
    subprocess.run(["bash", "install.sh"], check=True)

    num = 100

    out = subprocess.run(
        ["prettify-py", "format-dir", f"{test_run_dir}", "-n", f"{num}"],
        capture_output=True,
        check=True,
    )

    assert "ModuleNotFoundError: No module named 'lia'" not in str(out)
    assert "No such option:" not in str(out)

    check_delete_dir("tests/cli_files")
