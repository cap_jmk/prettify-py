import os
import collections
from prettify_py.split_line_cutoff import split_lines_after_len, format_file_line_length


def test_splitting_module():
    try:
        os.remove(new_file_name)
    except:
        pass

    file_name = "tests/check_files/split_lines.txt"
    new_file_name = "tests/check_files/split_lines_new.txt"
    num = 100

    format_file_line_length(
        file_name=file_name, new_file_name=new_file_name, cutoff=num
    )
    format_file_line_length(file_name=new_file_name, cutoff=num)


def test_split_lines_func():

    file_name = "tests/check_files/split_lines.txt"
    num = 100
    new_lines = split_lines_after_len(file_name=file_name, cutoff=num, pattern=" ")
    for line in new_lines:
        assert len(line) <= num
