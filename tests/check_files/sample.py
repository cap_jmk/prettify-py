"""
Module for formatting Python files 

:author: Julian M. Kleber 
"""
import os
import re
import subprocess


import click


@click.command()
@click.option("-d", help="Specify the directory you want to format the Python files.")
def format(d: str) -> None:
    """
    The format function is a wrapper for the format_docstrings and strip_whitespace functions.
        It takes in a string, d, which is the name of the directory containing all of your Python files.
        The function then calls both format_docstrings and strip_whitespace on each file in that directory.

    :param d:str: Used to Pass the docstring to the function.
    :return: None.

    :doc-author: Trelent
    """

    format_docstrings(d=d)
    strip_whitespace(d=d)


def format_docstrings(d: str) -> None:
    """
    The format_docstrings function takes a directory as an argument and formats all the docstrings in that directory.
        It does this by running the pydocstyle command on each file in the given directory, and then writing to a text file
        called "docstring_errors.txt" which contains any errors found by docformatter.

    :param f:str: Used to Specify the file name and the ->none parameter is used to specify that no output will be returned.
    :return: A list of the docstrings in each python file.

    :doc-author: Trelent
    """

    dir_content = os.listdir(d)
    py_files = [x for x in dir_content if x.endswith(".py")]
    for py_f in py_files:
        subprocess.run(["docformatter", py_f], stdout=f)


def strip_whitespace(d: str) -> None:
    """
    The format function takes a file path as an argument and recursively
    walks through the directory structure, looking for Python files.  When it
    finds one, it opens the file and strips trailing whitespace from each line.

    :param f:str: Used to Specify the file path of the directory you want to format.
    :return: None.

    :doc-author: Julian M. Kleber
    """

    re_strip = re.compile(r"[ \t]+(\n|\Z)")
    for path, dirs, files in os.walk(d):
        for f in files:
            file_name, file_extension = os.path.splitext(d)
            if file_extension == ".py":
                path_name = os.path.join(path, f)
                with open(path_name, "r") as fh:
                    data = fh.read()

                data = re_strip.sub(r"\1", data)

                with open(path_name, "w") as fh:
                    fh.write(data)


if __name__ == "__main__":
    strip_whitespace()
