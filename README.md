# prettify-py
[![Downloads](https://static.pepy.tech/personalized-badge/prettify-py?period=total&units=international_system&left_color=black&right_color=orange&left_text=Downloads)](https://pepy.tech/project/carate)
[![License: GPL v3](https://img.shields.io/badge/License-GPL_v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
![Python Versions](https://img.shields.io/badge/python-3.8%20%7C%203.9%20%7C%203.10%20%7C%203.11%20%7C%20-blue) 
![Style Black](https://warehouse-camo.ingress.cmh1.psfhosted.org/fbfdc7754183ecf079bc71ddeabaf88f6cbc5c00/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f636f64652532307374796c652d626c61636b2d3030303030302e737667) 


[![PyPI - Version](https://img.shields.io/pypi/v/prettify-py.svg)](https://pypi.org/project/prettify-py)


# Why 

Pretty Python files are necessary for productive Python development with high quality and safety standards. 

# What 

It is a simple command-line application formatting Python files written in Python. The package is yet highly experimental
but it was necessary to speed of maintenance and development speeds of software. It has not perfect stats on quality yet, but it is pretty good already. 

# Usage 

```bash
prettify-py format-file sample.py
prettify-py format-dir sample_dir/
```

# Installation

## Production Build 

```bash 
pip install prettify-py
```

## Dev Build
Clone the repository with

```bash 
git clone https://codeberg.org/cap_jmk/prettify-py
cd prettify-py/
```

### Linux 

Run with one of the follwing: 
```bash
bash install.sh
./install.sh
sh install.sh
```


### Windows

Double click on `install.bat` or run

```bash
install.bat
```