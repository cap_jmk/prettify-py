# Copyright Julian M. Kleber 

echo local-ci  Copyright (C) 2022 Julian M. Kleber This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'. This is free software, and you are welcome to redistribute it under certain conditions; type `show c' for details.

black prettify_py/
autopep8 --in-place --recursive prettify_py/
python -m flake8 prettify_py/ --count --select=E9,F63,F7,F82 --show-source --statistics
mypy --strict prettify_py/
python -m pylint -f parseable prettify_py/
pytest tests/